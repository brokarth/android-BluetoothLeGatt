package com.bsoto.android.client;

import com.bsoto.android.model.OxygenSaturation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VitalSignClient {

    @POST("vital-sign/oxygen-saturation")
    Call<Void> addVitalSign(@Body OxygenSaturation oxygenSaturation);
}
