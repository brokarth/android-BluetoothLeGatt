package com.bsoto.android.client;

import com.bsoto.android.model.OxygenSaturation;
import com.bsoto.android.model.VitalSign;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RestClient {
    private final static String BFF_URL = "http://192.168.0.6:8080/";
    private final static Retrofit RETROFIT = new Retrofit.Builder()
            .baseUrl(BFF_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .build();

    private final static ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final static int CALL_TIMEOUT = 5;

    private final VitalSignClient vitalSignClient;

    public RestClient() {
        vitalSignClient = RETROFIT.create(VitalSignClient.class);
    }

    public void sendVitalSigns(VitalSign vitalSign) {
        if(!(vitalSign instanceof OxygenSaturation)) {
            return;
        }

        Future<Void> futureResponse = EXECUTOR_SERVICE.submit(() -> {
            System.out.println("Enviando request a " + BFF_URL);
            vitalSignClient.addVitalSign((OxygenSaturation)vitalSign).execute();
            return null;
        });

        try {
            futureResponse.get(CALL_TIMEOUT, TimeUnit.SECONDS);

        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
