package com.bsoto.android.model;

import java.util.Date;

public class OxygenSaturation extends VitalSign {
    private int oxygenSaturationValue;
    public OxygenSaturation(Date creationTime, int oxygenSaturationValue, Worker worker, Location location) {
        super(creationTime, worker, location);
        this.oxygenSaturationValue = oxygenSaturationValue;
    }

    public int getOxygenSaturationValue() {
        return oxygenSaturationValue;
    }

    @Override
    public String toString() {
        return String.format("Saturacion de Oxigeno: %s", this.oxygenSaturationValue);
    }
}
