package com.bsoto.android.model;

import java.util.Date;

public class CardiacRhythm extends VitalSign {
    private int cardiacRhythmValue;
    public CardiacRhythm(Date creationTime, int cardiacRhythmValue, Worker worker, Location location) {
        super(creationTime, worker, location);
        this.cardiacRhythmValue = cardiacRhythmValue;
    }

    public int getCardiacRhythmValue() {
        return cardiacRhythmValue;
    }

    @Override
    public String toString() {
        return String.format("Ritmo Cardiaco: %s", this.cardiacRhythmValue);
    }
}
