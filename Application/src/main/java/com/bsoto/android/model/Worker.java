package com.bsoto.android.model;

public class Worker {
    private int rut;
    private String name;

    public Worker(int rut, String name) {
        this.rut = rut;
        this.name = name;
    }

    public int getRut() {
        return rut;
    }

    public String getName() {
        return name;
    }
}
