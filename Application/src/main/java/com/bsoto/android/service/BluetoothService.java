package com.bsoto.android.service;

import com.bsoto.android.client.RestClient;
import com.bsoto.android.model.BloodPressure;
import com.bsoto.android.model.CardiacRhythm;
import com.bsoto.android.model.Location;
import com.bsoto.android.model.OxygenSaturation;
import com.bsoto.android.model.VitalSign;
import com.bsoto.android.model.Worker;

import java.util.Date;

public class BluetoothService {
    private final static RestClient REST_CLIENT = new RestClient();

    private final static String CARDIAC_RHYTHM_FLAG = "AB";
    private final static String BLOOD_PRESSURE_FLAG = "B1";
    private final static String OXYGEN_SATURATION_FLAG = "B2";
    private final static int EXPECTED_DATA_LENGTH = 3;

    public String send(String smartWatchData) {
        return processData(smartWatchData);
    }

    private String processData(String smartWatchData){
        String message = smartWatchData;
        String[] bluetoothData = getHexadecimalValue(smartWatchData);
        if(bluetoothData != null) {
            VitalSign vitalSign = vitalSignBuilder(bluetoothData);

            sendToBff(vitalSign);

            message = vitalSign != null ? vitalSign.toString() : smartWatchData;
        }

        return message;
    }

    private void sendToBff(VitalSign vitalSign) {
        REST_CLIENT.sendVitalSigns(vitalSign);

    }

    private String[] getHexadecimalValue(String smartWatchData) {
        String[] splittedData = smartWatchData.split("\n");

        return splittedData.length != EXPECTED_DATA_LENGTH ? null : splittedData[2].split(" ");
    }

    private VitalSign vitalSignBuilder(String[] bluetoothData) {
        final int vitalSignTypeFlag = 10;
        final int radixTransformation = 16;

        VitalSign vitalSign = null;
        Location location = locationBuilder();
        Worker worker = getWorker();
        Date creationTime = new Date();
        switch (bluetoothData[vitalSignTypeFlag]){
            case CARDIAC_RHYTHM_FLAG:
                final int cardiacRhythmValue = Integer.parseInt(bluetoothData[bluetoothData.length - 1], radixTransformation);
                vitalSign = new CardiacRhythm(creationTime, cardiacRhythmValue, worker, location);
                break;
            case BLOOD_PRESSURE_FLAG:
                final int systolicPressure = Integer.parseInt(bluetoothData[bluetoothData.length - 3], radixTransformation);
                final int diastolicPressure = Integer.parseInt(bluetoothData[bluetoothData.length - 2], radixTransformation);
                vitalSign = new BloodPressure(creationTime, systolicPressure, diastolicPressure, worker, location);
                break;
            case OXYGEN_SATURATION_FLAG:
                final int oxygenSaturationValue = Integer.parseInt(bluetoothData[bluetoothData.length - 1], radixTransformation);
                vitalSign = new OxygenSaturation(creationTime, oxygenSaturationValue, worker, location);
            break;
            default:
        }

        return vitalSign;
    }

    private Worker getWorker() {
        return new Worker(172719082, "Benjamin");
    }

    private Location locationBuilder() {
        return new Location(0,0);
    }
}
